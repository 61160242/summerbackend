const mongoose = require('mongoose')
const Admin = require('./models/Admin')

mongoose.connect('mongodb://localhost:27017/mydb2', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})
Admin.find(function (err, admins) {
  if (err) return console.error(err)
  console.log(admins)
})

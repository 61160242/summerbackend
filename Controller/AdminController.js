const Admin = require('../models/Admin')
const adminController = {
  adminList: [
    {
      id: 1,
      first_name: 'Rujirada',
      last_name: 'Norasarn',
      gender: 'F',
      birthday: '1999-01-27',
      tel: '061-666-6666'
    },
    {
      id: 2,
      first_name: 'Kantapond',
      last_name: 'Aonraksa',
      gender: 'F',
      birthday: '2000-07-17',
      tel: '083-888-8888'
    }
  ],
  lastId: 3,
  async addAdmin (req, res) {
    const payload = req.body
    const admins = new Admin(payload)
    try {
      await admins.save()
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateAdmin (req, res) {
    const payload = req.body
    try {
      const admins = await Admin.updateOne({ _id: payload._id }, payload)
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteAdmin (req, res) {
    const { id } = req.params
    try {
      const admins = await Admin.deleteOne({ _id: id })
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAdmins (req, res) {
    try {
      const admins = await Admin.find({})
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getAdmin (req, res) {
    const { id } = req.params

    try {
      const admins = await Admin.findById(id)
      res.json(admins)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = adminController

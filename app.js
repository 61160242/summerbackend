const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/mydb2', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const helloRouter = require('./routes/hello')
const adminRouter = require('./routes/admin')
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/hello', helloRouter)
app.use('/admin', adminRouter)

module.exports = app
